#include "vec.h"

struct Camera {
  //pin hole position
  Point hole ;
  //camera direction
  Vector direction ;
  //aperture angle along the x axis
  float angle ;
  //camera rotation for 3D cameras
  Vector up ;
  //image resolution
  int resolution[2] ;

  //this method is for you to code
  Point pixel(int i, int j) ;
} ;
