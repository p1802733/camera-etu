#include "svg.h"
#include "sampling.h"
#include "pinhole.h"
#include "box.h"
#include "triangle.h"

#include "vec.h"
#include "mat.h"
#include "color.h"
#include "image_io.h"
#include "mesh_io.h"

#include <iostream>
#include <fstream>
#include <string>
#include <limits>
#include <algorithm>

//===== Running tests =====

//comment to activate / deactivate tests

//#define PINHOLE_2D_TEST
//#define BOX_2D_TEST
//#define PINHOLE_3D_TEST
//#define TRIANGLE_TEST
//#define ROBOT_TEST

//===== Utilities for plotting =====

static constexpr double res = 256 ;
static constexpr double pt_rad = 2 ;
static constexpr const char* stroke_width = "2" ;

std::string color_string(const Color& color) {
  std::stringstream ss ;
  ss << "rgb(" 
    << (int) (color.r * 256) << ","
    << (int) (color.g * 256) << ","
    << (int) (color.b * 256) << ")" ;
  return ss.str() ;
}

SVG::Circle* plot_point(
    SVG::Element* parent, 
    const Point& point, 
    double size = 1
    ) {
  SVG::Circle* c = parent->add_child<SVG::Circle>(
      res * point.x, 
      -res * point.y, 
      size*pt_rad
      ) ;
  c->set_attr("stroke-width", stroke_width) ;
  return c ;
}

SVG::Circle* plot_circle(
    SVG::Element* parent, 
    const Point& point, 
    double radius
    ) {
  SVG::Circle* c = parent->add_child<SVG::Circle>(
      res * point.x, 
      -res * point.y, 
      res*radius
      ) ;
  c->set_attr("fill", "none") ;
  c->set_attr("stroke", "black") ;
  c->set_attr("stroke-width", stroke_width) ;
  return c ;
}

SVG::Line* plot_segment(
    SVG::Element* parent, 
    const Point& from, 
    const Point& to
    ) {
  SVG::Line* l = parent->add_child<SVG::Line>(
      res * from.x, 
      res * to.x, 
      -res * from.y, 
      -res * to.y
      ) ;
  l->set_attr("stroke", "black") ;
  l->set_attr("stroke-width", stroke_width) ;
  return l ;
}

SVG::Rect* plot_box(
    SVG::Element* parent, 
    const Box& b
    ) {
  SVG::Rect* r = parent->add_child<SVG::Rect>(
      res * b.min.x, 
      -res * b.max.y, 
      res * (b.max.x - b.min.x),
      res * (b.max.y - b.min.y)
      ) ;
  r->set_attr("stroke-width", stroke_width) ;
  return r ;
}


//===== Testing =====

int main() {

#ifdef PINHOLE_2D_TEST
  {
    Camera c = {
      {0.1, 0.9, 0},
      {0.1, -0.07, 0},
      45,
      {0, 0, 1},
      {10, 10}
    } ;
    
    //initialize svg image
    SVG::SVG svg ;
    SVG::Element* grp = svg.add_child<SVG::Group>() ;

    //unit square
    SVG::Element* unit = plot_box(grp, {{0,0,0}, {1,1,1}}) ;
    unit->set_attr("stroke", "black") ;
    unit->set_attr("fill", "none") ;
    svg.autoscale() ;

    //plot camera
    SVG::Element* d = plot_segment(grp, c.hole, c.hole + 2*normalize(c.direction)) ;
    d->set_attr("stroke-width", "0.5") ;
    d->set_attr("stroke", "black") ;
    Transform rot = RotationZ(0.5 * c.angle) ;
    d = plot_segment(grp, c.hole, c.hole + 2*normalize(rot(c.direction))) ;
    d->set_attr("stroke-width", "0.5") ;
    d->set_attr("stroke", "black") ;
    Transform invrot = RotationZ(-0.5 * c.angle) ;
    d = plot_segment(grp, c.hole, c.hole + 2*normalize(invrot(c.direction))) ;
    d->set_attr("stroke-width", "0.5") ;
    d->set_attr("stroke", "black") ;
    plot_point(grp, c.hole) ;
    
    //plot pixels
    for(int i = 0; i < c.resolution[0]; ++i) {
      Point px = c.pixel(i, 0) ;
      plot_point(grp, px) ;
      if(i > 0) {
        Point prev = c.pixel(i-1, 0) ;
        Point mid = px + 0.5 * (prev - px) ;
        d = plot_segment(grp, c.hole, c.hole + 2 * normalize(mid - c.hole)) ;
        d->set_attr("stroke-width", "0.5") ;
        d->set_attr("stroke", "gray") ;
        d->set_attr("stroke-dasharray", "2,3") ;
      }
    }

    //export the final svg
    const char* outfile = "pinhole_2d.svg" ;
    std::ofstream file(outfile) ;
    file << std::string(svg) ;

    std::cout << "generated " << outfile << std::endl ;
  }
#endif

#ifdef BOX_2D_TEST
  {

    std::vector<Point> origins ;
    std::vector<Vector> directions ;
    std::vector<float> hits ;

    Box box = {{0.5, 0.2, 0}, {0.9, 0.6, 0}} ;
  
    int nb_rays = 200 ;
    for(int i = 0; i < nb_rays; ++i) {
      origins.emplace_back(rand_in_square()) ;
      directions.emplace_back(Vector(rand_in_circle())) ;
      hits.push_back(box.hit(origins.back(), directions.back())) ;
    }
    
    //initialize svg image
    SVG::SVG svg ;
    SVG::Element* grp = svg.add_child<SVG::Group>() ;

    //unit square
    SVG::Element* unit = plot_box(grp, {{0,0,0}, {1,1,1}}) ;
    unit->set_attr("stroke", "black") ;
    unit->set_attr("fill", "none") ;
    svg.autoscale() ;

    //plot box
    SVG::Element* test_box = plot_box(grp, box) ;
    test_box->set_attr("stroke", "black") ;
    test_box->set_attr("fill", "none") ;

    //plot rays
    for(int i = 0; i < nb_rays; ++i) {
      if(hits[i] < std::numeric_limits<float>::infinity()) {
        Point h = origins[i] + hits[i] * directions[i] ;
        SVG::Element* elt = plot_segment(grp, origins[i], h) ;
        elt->set_attr("stroke-width", "0.5") ;
        plot_point(grp, origins[i]) ;
        plot_point(grp, h) ;
      } else {
        SVG::Element* elt = plot_segment(grp, origins[i], origins[i] + 2*directions[i]) ;
        elt->set_attr("stroke-width", "0.5") ;
        elt->set_attr("stroke-dasharray", "2,3") ;
        elt->set_attr("stroke", "lightgray") ;
        elt = plot_point(grp, origins[i], 0.5) ;
        elt->set_attr("fill", "lightgray") ;
      }
    }
 
    //export the final svg
    const char* outfile = "box_2d.svg" ;
    std::ofstream file(outfile) ;
    file << std::string(svg) ;

    std::cout << "generated " << outfile << std::endl ;
  }
#endif

#ifdef PINHOLE_3D_TEST
  {
    //initialize an image
    Image im(512, 512) ;

    //initialize a camera
    Camera c = {
      {0.1, 0.9, 0.9},
      {0.9, -0.9, -0.9},
      45,
      {0, 0, 1},
      {im.width(), im.height()}
    } ;

    //initialize a box visible by the camera
    Box b = {
      {0.5, 0.1, 0.1},
      {0.9, 0.4, 0.5}
    } ;

    //storage for hits and hit bounds
    std::vector<float> hits(im.width() * im.height(), std::numeric_limits<float>::infinity()) ;
    float min_hit = std::numeric_limits<float>::infinity() ;
    float max_hit = 0 ;

    //compute hits for every pixel
    for(int i = 0; i < im.width(); ++i) {
      for(int j = 0; j < im.height(); ++j) {
        float h = b.hit(c.hole, c.pixel(i,j) - c.hole) ;
        if(h < std::numeric_limits<float>::infinity()) {
          //a hit was found, store it and update the bounds
          hits[i*im.height() + j] = h ;
          min_hit = std::min(min_hit, h) ;
          max_hit = std::max(max_hit, h) ;
        }
      }
    }

    //compute image gray scales
    for(int i = 0; i < im.width(); ++i) {
      for(int j = 0; j < im.height(); ++j) {
        float h = hits[i*im.height() + j] ;
        if(h < std::numeric_limits<float>::infinity()) {
          //use the hit offset normalized by the bounds in the range [0.2, 1]
          im(i, j) = Color(0.2 + 0.8 * (h - min_hit) / (max_hit - min_hit)) ;
        }
      }
    }

    //write the final image
    const char* outfile = "pinhole_3d.png" ;
    write_image(im, outfile) ;
    std::cout << "generated " << outfile << std::endl ;
  }
#endif

#ifdef TRIANGLE_TEST
  {
    //initialize an image
    Image im(512, 512) ;

    //initialize a camera
    Camera c = {
      {0.1, 0.9, 0.9},
      {0.9, -0.9, -0.9},
      45,
      {0, 0, 1},
      {im.width(), im.height()}
    } ;

    //load a set of triangles
    std::vector<Point> triangles ;

#ifndef ROBOT_TEST
    //single triangle in the simple test
    triangles.push_back({0.5, 0.1, 0}) ;
    triangles.push_back({0.9, 0.1, 0}) ;
    triangles.push_back({0.9, 0.5, 0}) ;
#else
    //a full mesh with multiple triangles
    const char* mesh_file = "data/robot.obj" ;
    if(!read_positions(mesh_file, triangles)) {
      std::cout << "error, " << mesh_file << " file not found, ensure the working directory is gKit3 root" << std::endl ;
    }

    Transform robot_setup = 
      Translation(1, 0, -0.5) 
      * Scale(0.2) 
      * RotationZ(210)
      * RotationX(90)
    ;
    for(Point& p : triangles) {
      p = robot_setup(p) ;
    }
#endif

    //triangles bounding box to improve performance a bit
    Box bbox = {triangles[0], triangles[0]} ;
    for(const Point&p : triangles) {
      bbox.min = min(bbox.min, p) ;
      bbox.max = max(bbox.max, p) ;
    }

    //storage for hit data and bounds
    std::vector<float> hits(im.width() * im.height(), std::numeric_limits<float>::infinity()) ;
    float min_hit = std::numeric_limits<float>::infinity() ;
    float max_hit = 0 ;

    //compute hits for every pixel
    for(int i = 0; i < im.width(); ++i) {
      for(int j = 0; j < im.height(); ++j) {
        //ray direction and reference pixel
        Vector direction = c.pixel(i,j) - c.hole ;
        float& result = hits[i*im.height() + j] ;
        //try the bounding box first
        float bh = bbox.hit(c.hole, direction) ;
        if(bh < std::numeric_limits<float>::infinity()) {
          //hit the bounding box, try the mesh
          for(std::size_t t = 0; t < triangles.size(); t+=3) {
            float h = triangle_hit(c.hole, direction, triangles.data() + t) ;
            if(h < std::numeric_limits<float>::infinity()) {
              //a new hit was found, keep only the nearest one for this ray
              result = std::min(result, h) ;
              //update the bounds
              min_hit = std::min(min_hit, result) ;
              max_hit = std::max(max_hit, result) ;
            }
          }
        }
      }
    }

    //compute image gray scales
    for(int i = 0; i < im.width(); ++i) {
      for(int j = 0; j < im.height(); ++j) {
        float h = hits[i*im.height() + j] ;
        if(h < std::numeric_limits<float>::infinity()) {
          im(i, j) = Color(0.2 + 0.8 * (h - min_hit) / (max_hit - min_hit)) ;
        }
      }
    }

    //write final image
    const char* outfile = "triangle_soup.png" ;
    write_image(im, outfile) ;
    std::cout << "generated " << outfile << std::endl ;
  }
#endif

  return 0 ;
}
