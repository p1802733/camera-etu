# TP Camera

Ce TP a pour but de mettre en place une caméra pour visualiser une scène 3D.

## Mise en route

Dans votre arborescence de 
[gKit3](https://forge.univ-lyon1.fr/JEAN-CLAUDE.IEHL/gkit3)
naviguez dans le dossier `projets` et clonez ce dépôt. Éditez ensuite le fichier
`premake4.lua` à la racine de gKit3 en ajoutant à la fin les lignes suivantes :

```lua
project("camera")
  language "C++"
  kind "ConsoleApp"
  targetdir "bin"
  buildoptions ( "-std=c++17" )
  files ( gkit_files )
  files { "projets/camera-etu/src/*.cpp" }
```

Générez ensuite les fichiers de compilation (voir le readme de gKit3) par
exemple pour linux + Makefile via la commande

```
premake5 gmake
```



## Caméra 2D

Le but de cette partie est de comprendre les paramètres d'une caméra *pinhole*
(trou d'épingle en français). Mécaniquement, ce dispositif consiste à prendre
une boîte hermétique ne laissant pas passer la lumière. À l'intérieur de cette
boîte, centré sur une des faces, on fixe un film photographique. On peut considérer que
ce film est noir de base, et qu'il prend la couleur de la lumière qu'il reçoit.
Sur la face opposée de la boîte, on perce au centre un tout petit trou avec une
épingle, qui permettra à la lumière de pénétrer dans la boîte. Pour atteindre le
film, un rayon lumineux devra passer par le trou. La lumière circulant en ligne
droite, si on trace le rayon qui part d'un point du film et qui sort par le trou
d'épingle, le premier objet rencontré sera celui qui enverra la lumière captée
par le film. 

Notre but dans cette première partie est de reproduire virtuellement cette
expérience dans une scène 2D, avec un film 1D. Les films d'antan étaient enduits
d'une couche de grains de nitrate d'argent. Ces grains réagissaient avec la
lumière, et devaient être répartis uniformément pour reconstituer une image.
Dans notre cadre d'images numériques, ces grains de nitrate d'argent sont
remplacés par des *pixels* disposés régulièrement selon une grille.

![modele pinhole](img/pinhole_model.svg)

Dans cette configuration, l'image obtenue est inversée par rapport à la scène :
les pixels du bas du film correspondent à la zone du haut de la scène et
vice-versa. Pour éviter cette inversion, on peut réaliser une symétrie centrale
par rapport au trou d'épingle, ce qui revient à placer un film virtuel devant le
trou.

![symétrie pinhole](img/pinhole_symetry.svg)

Votre but dans cette première partie est de de placer dans l'espace les pixels
de ce film virtuel.

### Paramètres de la caméra

Une structure de base de caméra est fournie dans la paire de fichiers `pinhole`.
Cette structure comporte les champs :

* `hole` qui est la position dans l'espace du trou d'épingle ;
* `direction` la normale de la face de la boîte avec le trou d'épingle ;
* `angle` est l'ouverture de la caméra (voir le schéma) ;
* `up` est inutile pour cette partie ;
* `resolution` est le nombre de pixels de l'image : `[largeur, hauteur]`.

![parametres pinhole](img/pinhole_parameters.svg)

Notez que certains de ces paramètres sont pour l'instant inutiles pour une
caméra dans une scène 2D avec un film 1D : le vecteur `up` ainsi que la hauteur
de l'image.

### Votre travail

Implémentez la méthode `pixel` de la classe `Camera`. Cette méthode prend en
paramètre les indices du pixel qu'on souhaite positionner dans la scène. Pour
cette première partie, l'indice `j` est inutile. Pour positionner les pixels,
vous pouvez choisir librement la distance entre le trou d'épingle et la droite
contenant les pixels. Quelques conseils :

* prenez une feuille, dessinez et faites les maths à la main ;
* pensez à la tangente de la moitié de l'angle d'ouverture ;
* chaque pixel est au centre de son intervalle ;
* ne bloquez pas trop, demandez de l'aide.

Une fois implémentée, vous pouvez activer un test en
décommantant la ligne suivante au début du fichier `main.cpp` :

```cpp
#define PINHOLE_2D_TEST
```

Si tout se passe bien, vous devriez obtenir l'image suivante :

![resultat pinhole 2d](img/pinhole_2d.svg)

## Intersection rayon boîte

Pour déterminer la couleur des pixels, il nous faut maintenant déterminer quels
sont les objets visibles pour chaque pixel. Nous avons donc besoin d'objets pour
lesquels il est simple de calculer le point d'impact avec un rayon partant du
trou d'épingle et passant par un pixel. Dans cette
partie, nous vous proposons d'utiliser une *boîte alignée sur les axes* : il
s'agit en 2D d'un rectangle donc les côtés sont parallèles aux axes x et y. Un
tel rectangle peut facilement être représenté par deux points `min` et `max`. Un
point `p` est dans la boîte si ses coordonnées sont telles que 
`min.x < p.x < max.x` et `min.y < p.y < max.y`.

![boîte alignée](img/aabb.svg)

Un rayon coupe la boîte s'il contient un point dedans. Nous allons donc chercher
la portion du rayon qui se trouve dans la boîte, si elle existe. Plus
précisément, le rayon est défini par :

* son *origine* `o` ;
* sa *direction* `d`.

Les ponts du rayons sont l'ensemble des points `o + t*d` avec
`t` un nombre réel positif (les négatifs sont derrière la caméra). Pour
déterminer l'intersection du rayon avec le plan $x = a$, il suffit alors de
trouver le `t` qui résout l'équation `o.x + t*d.x = a`. En trouvant les deux `t`
correspondant à `min.x` et `max.x`, on obtient la plage du rayon pour laquelle
les points ont une coordonnée `x` qui leur permet d'être dans la boîte. En
itérant le procédé sur chaque dimension, il est possible de réduire
l'intervalle. Si à la fin il reste des points possibles, et s'ils sont devant la
caméra, alors il y a intersection.

### Votre travail

Les boîtes sont fournies dans la paire de fichiers `box`. Dans `box.cpp`
implémentez la méthode `hit`. Cette méthode renvoie le `t` tel que 
`origin + t * direction` est le point d'intersection avec la boîte. Attention :

* sur chaque dimension, le `t` du min n'est par forcément plus petit que le `t`
  du max ;
* il est possible que l'un des deux `t` soit négatif sans que l'autre le soit ;
* lorsque l'origine est dans la boîte, le `t` cherché est le point de sortie.

Une fois la fonction implémentée, vous pouvez activer le test en décommentant la
ligne

```cpp
#define BOX_2D_TEST
```

Si tout se passe bien, vous devriez obtenir l'image suivante :

![resultat box 2d](img/box_2d.svg)

## Passage en 3D

Pour passer en 3D, il est nécessaire de rajouter des paramètres à la caméra :
connaissant la position du trou d'épingle et la direction d'observation, il est
encore possible de la faire tourner autour de l'axe de la direction. Pour fixer
l'assiette de la caméra, nous ajoutons un vecteur `up` à la caméra. Attention,
pour des raisons pratiques ce vecteur n'est pas forcément orthogonal à la
direction de la caméra. Dites vous que le plan contenant les vecteurs `direction`
et `up` tranche votre caméra de bas en haut. En pratique, si vous aimez prendre
des photos horizontales, dites vous que le vecteur `up` est dans le sens opposé
à la gravité.

Dans ce contexte, l'angle d'ouverture de la caméra est un angle *horizontal*
c'est à dire qu'il correspond à la *largeur* du film. Les pixels sont ensuite
positionnés verticalement en respectant le ratio d'aspect de l'image.

![prametres pinhole 3d](img/pinhole_3d_parameters.svg)

### Votre travail

Reprenez la méthode `pixel` de la classe `Camera` pour désormais placer les
pixels en 3D. Quelques conseils :

* utilisez des produits vectoriels pour générer des vecteurs orthogonaux ;
* créez une base orthogonale du plan contenant les pixels ;

Une fois la fonction implémentée, vous pouvez activer le test en décommentant la
ligne

```cpp
#define PINHOLE_3D_TEST
```

Si tout se passe bien, vous devriez obtenir l'image suivante :

![resultat pinhole 3d](img/pinhole_3d.png)

## Intersection rayon triangle

Étant donné un triangle représenté par trois points `p0`, `p1` et `p2` et un
rayon représenté par son origine `o` et sa direction `d` il est possible de
regarder de quel côté la direction se trouve du plan défini par trois points
`o`, `pi` et `pj`. Ce test peut être réalisé en calculant le déterminant de la
matrice donc les colonnes sont les vecteurs `pi - o`, `pj - o` et `d`. Ce
déterminant peut être calculé via


```
dot(cross(o - pi, o - pj), d)
```

Intuitivement ce test revient à calculer la normale du triangle `(o, pi, pj)` et
à regarder ensuite si la direction `d` va dans le même sens que cette normale ou
non.

En réalisant ce test pour les trois arêtes du triangle, le rayon coupe le
triangle si les trois tests ont le même signe. Selon que le triangle est face à
la caméra ou lui tourne le dos ce signe pourra être positif ou négatif.

Une fois l'intersection déterminer, il reste à déterminer le point
d'intersection. Les déterminants calculés précédemment fournissent en réalités
les *coordonnées barycentriques* de ce point par rapport aux sommets du
triangle. En effet, si on pose `det0`, `det1` et `det2` de sorte que

```
det0 = dot(cross(o - p0, o - p1), d)
```
alors la coordonnées barycentrique du point d'impact par rapport au sommet `p0`
est donnée par la relation

```
a0 = det0 / (det0 + det1 + det2)
```

Le point d'impact est donc `p = a0*p0 + a1*p1 + a2*p2`.

### Votre travail

Dans la paire de fichiers `triangle` implémentez la fonction `triangle_hit` qui
prend en paramètre les données du rayon ainsi qu'un tableau contenant les trois
points du triangle. Cette fonction renvoie le `t` tel que `origin + t *
direction` est le point d'impact du rayon avec le triangle, s'il existe. Sans
intersection, elle renvoie l'infini. Quelques conseils :

* la direction du rayon n'est pas forcément normalisée ;
* le `t` à renvoyer dépend de la norme de la direction ;

Une fois la fonction implémentée, vous pouvez activer le test en décommentant la
ligne

```cpp
#define TRIANGLE_TEST
```

Si tout se passe bien, vous devriez obtenir l'image suivante :

![resultat triangle simple](img/single_triangle.png)

Vous pouvez alors décommenter le test

```cpp
#define ROBOT_TEST
```

pour obtenir l'image suivante :

![resultat robot](img/robot.png)

## À vous

Pour poursuivre ce travail, vous pouvez implémenter d'autres primitives,
chercher à accélérer le calcul, ou encore positionner une lumière dans la scène
et essayer de représenter la vraie couleur des objets et non simplement leur
profondeur.
